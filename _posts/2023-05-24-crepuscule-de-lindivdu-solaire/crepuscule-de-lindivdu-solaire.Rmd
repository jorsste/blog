---
title: "Crépuscule de l'individu solaire"
description: |
  Réflexion sur les personnages nietzschéens dans la littérature
author:
  - name: Jean Orsste
    url: mailto:jorsste@protonmail.com
date: 2023-05-24
output:
  distill::distill_article:
    self_contained: false
draft: false
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

*Solaire* : [En parlant d'une personne] Rayonnant de bonheur, de joie
de vivre.

La littérature laisse une place importante à la figure de l'individu
solaire. *Les noces* de Camus en est sans doute le meilleur exemple que
j'ai en tête. Mais je pense également aux personnages d'Alain Damasio, qui sont pour moi plus problématiques tant j'approuve par ailleurs son message. Le personnage solaire est un individu dont la jouissance
face à ce qu'il lui est donné de vivre est totale et entière. Une
acceptation totale à la vie, qui se présente dans sa forme la plus
dispendieuse. Le grand « oui » nietzschéen. Une simplicité de
l'existence, qui tel un rasoir d'Ockham existentialiste, révèle toute
sa grandeur. Tout y est mouvement, vivacité et spontanéité. Les sens
sont pleinement sollicités. L'individu solaire ressent la vie. C'est
pour lui une expérience profondément immanente, loin des discours
abstraits, surplombants et ronflants des philosophes de salon. Le réel
n'est pas une idée pour l'individu solaire, c'est une sensation qui
élève plus haut encore l'âme. Ni réaction biophysique ni exaltation
pure. La vie relie les atomes les plus concrets avec les idées les plus
abstraites ; le toucher avec la joie ; les odeurs avec la liberté ; le
vent avec l'amour.

Nul ne peut nier que la personnalité solaire est désirable. Généreuse et
sincère, dans la douceur comme dans la fureur. On ne lui reconnaît
d'excès que comme marques d'authenticité. L'individu solaire est
juste. Non pas au sens d'« équitable » ou de « précis ». Encore moins
dans le sens de « limité » ou d'« étriqué ». Il est juste, car il
réagit aux variations de l'existence sans filtre, sans se cacher, sans
tromper. Il se donne entièrement à la vie, sans filet de sécurité.

Voilà le portrait de l'individu solaire. Il est flatteur, tout autant
qu'exigeant. Peu nombreux sont --- à mon sens --- les élus potentiels.
Rares sont les personnes que j'ai pu rencontrer au cours de ma vie qui
mériterait ce titre. En ai-je seulement déjà rencontrer ?

Je ne me reconnais d'ailleurs absolument pas dans cette personnalité.
Trop frugal, pas assez généreux. Trop froid, pas assez empathique. Trop
casanier, pas assez aventureux. Trop timoré, pas assez audacieux.
L'individu solaire relève en réalité du mythe. Personne ne peut
prétendre à ce titre. Mais comme tout mythe, il structure les
représentations. L'individu solaire, à défaut d'exister, met une
pression sur chacun de nous. La société nous invite à un courage
imbécile, à parler franchement pour dire franchement n'importe quoi, à
s'obstiner. C'est une invitation à l'immodestie. Seul compte
soi-même, pensé comme un empire qui doit s'étendre sur le monde.
Qu'importe que notre contribution au monde relève du génie ou de la
pire médiocrité, seul compte le fait d'avoir tracé notre route.

Plus grave encore : est-ce que l'individu solaire ne serait pas
finalement une figure conservatrice ? Avec son désire éperdu de liberté
(qu'il confond d'ailleurs parfois avec l'abondance) ? Son hédonisme
qui n'a rien d'un épicurisme ? Qu'a-t-il fait des structures sociales
? Qu'il les méprise est une chose, qu'il les dépasse et les défie est
vénérable, mais on ne peut pas faire comme si elles n'existaient pas.
Que fait-il de la solidarité avec les autres ? Sa spontanéité confine
souvent à l'individualisme. C'est un bourgeois qui n'accepte rien
au-dessus de lui, veut voir tout le monde à ses pieds. Il est prêt à
asservir le monde pour être seul empereur. Il exploite autant qu'il le
peut son environnement. Et évidemment, méprise ceux qu'ils l'appellent à
plus de modestie, de mesure et de sobriété - pure moraline à ses
yeux\...

Le mythe de l'individu solaire a vécu, nous pouvons passer à autre
chose.


